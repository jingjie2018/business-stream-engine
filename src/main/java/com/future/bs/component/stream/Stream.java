package com.future.bs.component.stream;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import com.future.bs.component.business.Business;
import com.future.bs.context.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * <p>
 * 抽象业务流
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 10:58
 */
public abstract class Stream<T extends Context> implements Business<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(Stream.class);

    protected String name;

    public Stream() {
        this.name = IdUtil.fastSimpleUUID();
    }

    public String getName() {
        return name;
    }

    @Override
    public void rollback(T context) {
        List<Business> executedBusiness = context.getExecutedBusiness();
        if (CollectionUtil.isEmpty(executedBusiness)) {
            LOGGER.warn("");
            return;
        }
        // 倒序回滚
        for (int index = executedBusiness.size() - 1; index >= 0; index--) {
            Business business = executedBusiness.get(index);
            business.rollback(context);
        }
    }
}
