package com.future.bs.component.business;

import com.future.bs.context.Context;
import com.future.bs.component.report.DefaultReport;
import com.future.bs.component.report.Report;
import com.future.bs.context.BusinessStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Nothing required to do for this business.
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 10:56
 */
public class DefaultBusiness<T extends Context> extends AbstractBusiness<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBusiness.class);

    private static final DefaultBusiness INSTANCE = new DefaultBusiness();

    public static DefaultBusiness getInstance() {
        return INSTANCE;
    }

    @Override
    public Report<T> doExecute(T context) {
        LOGGER.info("Nothing required to do for the business '{}'");
        return new DefaultReport<>(BusinessStatus.SUCCESS, context);
    }
}
