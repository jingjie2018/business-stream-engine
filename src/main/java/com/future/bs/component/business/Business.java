package com.future.bs.component.business;

import com.future.bs.context.Context;
import com.future.bs.component.report.Report;

/**
 * <p>
 * 业务
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-07 21:20
 */
public interface Business<T extends Context> {
    /**
     * 执行业务流
     *
     * @param context 业务流上下文
     * @return 执行结果
     */
    Report<T> execute(T context);

    /**
     * 回滚
     *
     * @param context 业务流上下文
     */
    default void rollback(T context) {
        // default required nothing to do
    }
}
