package com.future.bs.component.business;

import com.future.bs.context.Context;
import com.future.bs.component.report.Report;

/**
 * <p>
 * 抽象业务
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 09:09
 */
public abstract class AbstractBusiness<T extends Context> implements Business<T> {
    @Override
    public Report<T> execute(T context) {
        // 保存当前业务到已执行任务列表中
        context.addExecutedBusiness(this);
        // 向前执行
        return doExecute(context);
    }

    /**
     * 执行业务
     *
     * @param context 上下文
     * @return 业务结果
     */
    public abstract Report<T> doExecute(T context);
}
