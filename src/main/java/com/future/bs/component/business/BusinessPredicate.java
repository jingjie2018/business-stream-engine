package com.future.bs.component.business;

import com.future.bs.component.report.Report;
import com.future.bs.context.BusinessStatus;
import com.future.bs.context.Context;

/**
 * <p>
 * 业务流断言
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 11:52
 */
@FunctionalInterface
public interface BusinessPredicate<T extends Context> {
    /**
     * always true
     */
    BusinessPredicate ALWAYS_TRUE = workReport -> true;

    /**
     * always false
     */
    BusinessPredicate ALWAYS_FALSE = workReport -> false;

    /**
     * success
     */
    BusinessPredicate SUCCESS = workReport -> workReport.getStatus().equals(BusinessStatus.SUCCESS);

    /**
     * failed
     */
    BusinessPredicate FAILED = workReport -> workReport.getStatus().equals(BusinessStatus.FAILED);

    /**
     * 断言
     *
     * @param report 业务流结果
     * @return true断言成立，false断言不成立
     */
    boolean apply(Report<T> report);
}
