package com.future.bs.component.report;

import com.future.bs.context.Context;
import com.future.bs.context.BusinessStatus;

/**
 * <p>
 * 业务流执行结果
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-07 21:20
 */
public interface Report<T extends Context> {
    /**
     * 业务流状态
     *
     * @return 业务流状态
     */
    BusinessStatus getStatus();

    /**
     * 业务流执行过程中的异常
     *
     * @return 异常
     */
    Throwable getError();

    /**
     * 业务流上下文
     *
     * @return 业务流上下文
     */
    T getContext();
}
