package com.future.bs.component.report;

import com.future.bs.context.Context;
import com.future.bs.context.BusinessStatus;

/**
 * <p>
 * 业务流结果默认实现
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 09:43
 */
public class DefaultReport<T extends Context> implements Report<T> {
    protected BusinessStatus status;
    protected T context;
    protected Throwable error;

    public DefaultReport() {
    }

    public DefaultReport(BusinessStatus status, T context) {
        this.status = status;
        this.context = context;
    }

    public DefaultReport(BusinessStatus status, T context, Throwable error) {
        this(status, context);
        this.error = error;
    }

    @Override
    public BusinessStatus getStatus() {
        return status;
    }

    @Override
    public Throwable getError() {
        return error;
    }

    @Override
    public T getContext() {
        return context;
    }
}
