package com.future.bs.context;

import com.future.bs.component.business.Business;

import java.util.List;

/**
 * <p>
 * 业务流上下文
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-07 21:20
 */
public interface Context {
    /**
     * save value
     *
     * @param key   键
     * @param value 值
     */
    void put(String key, Object value);

    /**
     * get value
     *
     * @param key 键
     * @return value
     */
    Object get(String key);

    /**
     * 获取已经执行业务
     *
     * @return 已执行业务
     */
    List<Business> getExecutedBusiness();

    /**
     * 保存已经执行业务
     *
     * @param business 当前执行的业务
     */
    void addExecutedBusiness(Business business);
}
