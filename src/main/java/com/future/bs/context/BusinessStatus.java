package com.future.bs.context;

/**
 * <p>
 * 业务流状态
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 09:13
 */
public enum BusinessStatus {
    /**
     * 业务流执行成功
     */
    SUCCESS,

    /**
     * 业务流执行失败
     */
    FAILED
}
