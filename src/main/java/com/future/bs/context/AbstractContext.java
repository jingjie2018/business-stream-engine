package com.future.bs.context;

import com.future.bs.component.business.Business;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>
 * 业务上下文默认实现
 * </p >
 *
 * @author Jingjie
 * @since 2020-11-10 09:38
 */
public abstract class AbstractContext implements Context {
    private final Map<String, Object> context = new ConcurrentHashMap<>();

    private final List<Business> executedBusinesses = Collections.synchronizedList(new LinkedList<>());

    @Override
    public void put(String key, Object value) {
        context.put(key, value);
    }

    @Override
    public Object get(String key) {
        return context.get(key);
    }

    @Override
    public void addExecutedBusiness(Business business) {
        this.executedBusinesses.add(business);
    }

    @Override
    public List<Business> getExecutedBusiness() {
        return executedBusinesses;
    }
}
