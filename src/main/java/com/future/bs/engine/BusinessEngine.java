package com.future.bs.engine;

import com.future.bs.component.business.Business;
import com.future.bs.component.report.Report;
import com.future.bs.context.Context;

/**
 * <p>
 * 业务流引擎
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-07 21:18
 */
public interface BusinessEngine<T extends Context> {
    /**
     * 执行业务流得到结果
     *
     * @param businessStream 业务流
     * @param context      业务流上下文
     * @return 业务执行结果
     */
    Report<T> execute(Business<T> businessStream, T context);
}
