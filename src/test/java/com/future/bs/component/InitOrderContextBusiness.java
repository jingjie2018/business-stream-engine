package com.future.bs.component;

import com.future.bs.bean.OrderContext;
import com.future.bs.bean.PriceCalcReqVO;
import com.future.bs.bean.ProductPackVO;
import com.future.bs.component.business.AbstractBusiness;
import com.future.bs.component.report.DefaultReport;
import com.future.bs.component.report.Report;
import com.future.bs.context.BusinessStatus;
import lombok.extern.slf4j.Slf4j;

import java.math.RoundingMode;
import java.text.MessageFormat;

/**
 * <p>
 * init order business context business
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 21:26
 */
@Slf4j
public class InitOrderContextBusiness extends AbstractBusiness<OrderContext> {
    @Override
    public Report<OrderContext> doExecute(OrderContext context) {
        // 初始化操作(db、数据组装)
        StringBuilder logStr = new StringBuilder();
        PriceCalcReqVO priceCalcReqVO = context.getPriceCalcReqVO();
        logStr.append(MessageFormat.format("订单号[{0}]的价格计算的明细结果:\n",
                priceCalcReqVO.getOrderNo()));
        logStr.append("|====================================================================\n");
        for (ProductPackVO pack : priceCalcReqVO.getProductPackList()) {
            logStr.append(MessageFormat.format("|   {0} [{1}] [{2}]    {3} * {4}\n",
                    pack.getSkuName(),
                    pack.getProductCode(),
                    pack.getSkuCode(),
                    pack.getSalePrice().setScale(2, RoundingMode.HALF_UP).toString(),
                    pack.getCount()));
        }
        logStr.append("|====================================================================\n");
        log.info(logStr.toString());
        log.info("初始化阶段：pass.");
        return new DefaultReport<>(BusinessStatus.SUCCESS, context);
    }

    @Override
    public void rollback(OrderContext context) {
        log.info("回滚：InitOrderContextBusiness");
    }
}
