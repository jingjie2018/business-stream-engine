package com.future.bs.component;

import com.future.bs.bean.OrderContext;
import com.future.bs.component.business.AbstractBusiness;
import com.future.bs.enums.OrderChannelEnum;
import com.future.bs.component.report.DefaultReport;
import com.future.bs.component.report.Report;
import com.future.bs.context.BusinessStatus;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 前置校验
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 19:22
 */
@Slf4j
public class CheckOrderChannelBusiness extends AbstractBusiness<OrderContext> {
    @Override
    public Report<OrderContext> doExecute(OrderContext context) {
        OrderChannelEnum orderChannel = context.getPriceCalcReqVO().getOrderChannel();
        if (orderChannel != OrderChannelEnum.APP) {
            log.info("校验商品渠道：fail.");
            return new DefaultReport<>(BusinessStatus.FAILED, context);
        }
        log.info("校验商品渠道：pass.");
        return new DefaultReport<>(BusinessStatus.SUCCESS, context);
    }

    @Override
    public void rollback(OrderContext context) {
        log.info("回滚：CheckOrderChannelBusiness");
    }
}
