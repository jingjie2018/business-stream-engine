package com.future.bs.component;

import com.future.bs.bean.OrderContext;
import com.future.bs.component.business.AbstractBusiness;
import com.future.bs.component.report.DefaultReport;
import com.future.bs.component.report.Report;
import com.future.bs.context.BusinessStatus;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 预锁库存 Pre lock inventory
 * </p >
 *
 * @author Jingjie
 * @since 2020-11-11 15:10
 */
@Slf4j
public class PreLockInventoryBusiness extends AbstractBusiness<OrderContext> {
    @Override
    public Report<OrderContext> doExecute(OrderContext context) {
        log.info("执行预锁库存");
        return new DefaultReport<>(BusinessStatus.SUCCESS, context);
    }

    @Override
    public void rollback(OrderContext context) {
        log.info("回滚：PreLockInventoryBusiness");
    }
}
