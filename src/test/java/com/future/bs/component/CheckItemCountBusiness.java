package com.future.bs.component;

import com.future.bs.bean.OrderContext;
import com.future.bs.bean.ProductPackVO;
import com.future.bs.component.business.AbstractBusiness;
import com.future.bs.component.report.DefaultReport;
import com.future.bs.component.report.Report;
import com.future.bs.context.BusinessStatus;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * item count check business
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 21:17
 */
@Slf4j
public class CheckItemCountBusiness extends AbstractBusiness<OrderContext> {
    @Override
    public Report<OrderContext> doExecute(OrderContext context) {
        for (ProductPackVO productPackVO : context.getPriceCalcReqVO().getProductPackList()) {
            Integer count = productPackVO.getCount();
            if (count < 1) {
                log.info("校验商品数量：fail.");
                return new DefaultReport<>(BusinessStatus.FAILED, context);
            }
        }
        log.info("校验商品数量：pass.");
        return new DefaultReport<>(BusinessStatus.SUCCESS, context);
    }


    @Override
    public void rollback(OrderContext context) {
        log.info("回滚：CheckItemCountBusiness");
    }
}
