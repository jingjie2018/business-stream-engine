package com.future.bs.component;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.future.bs.bean.*;
import com.future.bs.component.business.AbstractBusiness;
import com.future.bs.component.report.DefaultReport;
import com.future.bs.component.report.Report;
import com.future.bs.context.BusinessStatus;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 * promotion convert business
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 22:17
 */
@Slf4j
public class PromotionConvertBusiness extends AbstractBusiness<OrderContext> {
    @Override
    public Report<OrderContext> doExecute(OrderContext context) {
        List<PromotionPackVO> promotionPackList = new ArrayList<>();
        PromotionPackVO promotionPack;
        PriceCalcReqVO priceCalcReq = context.getPriceCalcReqVO();
        for (ProductPackVO pack : priceCalcReq.getProductPackList()) {
            if (CollectionUtil.isEmpty(pack.getPromotionList())) {
                continue;
            }
            for (PromotionInfoVO promotion : pack.getPromotionList()) {
                promotionPack = new PromotionPackVO();
                promotionPack.setId(promotion.getId());
                if (promotionPackList.contains(promotionPack)) {
                    promotionPack = promotionPackList.get(promotionPackList.indexOf(promotionPack));
                    if (!promotionPack.getRelatedProductPackList().contains(pack)) {
                        promotionPack.getRelatedProductPackList().add(pack);
                    }
                    continue;
                }
                BeanUtil.copyProperties(promotion, promotionPack);
                promotionPack.setRelatedProductPackList(new ArrayList<>(Collections.singletonList(pack)));
                promotionPackList.add(promotionPack);
            }
        }
        context.setPromotionPackList(promotionPackList);
        log.info("促销");
        return new DefaultReport<>(BusinessStatus.SUCCESS, context);
    }

    @Override
    public void rollback(OrderContext context) {
        log.info("回滚：PromotionConvertBusiness");
    }
}
