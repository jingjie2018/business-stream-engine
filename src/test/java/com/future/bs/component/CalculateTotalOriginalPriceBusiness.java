package com.future.bs.component;

import com.future.bs.bean.OrderContext;
import com.future.bs.bean.ProductPackVO;
import com.future.bs.component.business.AbstractBusiness;
import com.future.bs.component.report.DefaultReport;
import com.future.bs.component.report.Report;
import com.future.bs.context.BusinessStatus;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * total price business
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 19:54
 */
@Slf4j
public class CalculateTotalOriginalPriceBusiness extends AbstractBusiness<OrderContext> {
    @Override
    public Report<OrderContext> doExecute(OrderContext context) {
        List<ProductPackVO> productPackList = context.getPriceCalcReqVO().getProductPackList();
        BigDecimal totalOriginalPrice = new BigDecimal(0);
        for (ProductPackVO packItem : productPackList) {
            totalOriginalPrice = totalOriginalPrice.add(packItem.getSalePrice().multiply(new BigDecimal(packItem.getCount())));
        }
        context.setOriginalOrderPrice(totalOriginalPrice);
        log.info("计算原始总价：{}", totalOriginalPrice);
        return new DefaultReport<>(BusinessStatus.SUCCESS, context);
    }

    @Override
    public void rollback(OrderContext context) {
        log.info("回滚：CalculateTotalOriginalPriceBusiness");
    }
}
