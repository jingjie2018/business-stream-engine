package com.future.bs.component;

import com.future.bs.bean.OrderContext;
import com.future.bs.component.business.AbstractBusiness;
import com.future.bs.component.report.DefaultReport;
import com.future.bs.component.report.Report;
import com.future.bs.context.BusinessStatus;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 23:31
 */
@Slf4j
public class MemberDiscountBusiness extends AbstractBusiness<OrderContext> {
    @Override
    public Report<OrderContext> doExecute(OrderContext context) {
        // 8折
        BigDecimal price = context.getOriginalOrderPrice().multiply(new BigDecimal(0.8D));
        context.setFinalOrderPrice(price);
        log.info("会员8折，折后价：{}", price);
        int i = 1 / 0;
        return new DefaultReport<>(BusinessStatus.SUCCESS, context);
    }

    @Override
    public void rollback(OrderContext context) {
        log.info("回滚：MemberDiscountBusiness");
    }
}
