package com.future.bs.bean;

import com.future.bs.context.AbstractContext;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 订单业务上下文
 * </p>
 *
 * @author Jingjie
 * @since 2020-11-08 19:31
 */
public class OrderContext extends AbstractContext {
    private PriceCalcReqVO priceCalcReqVO;

    private List<PromotionPackVO> promotionPackList;

    private BigDecimal originalOrderPrice;

    private BigDecimal finalOrderPrice;

    public PriceCalcReqVO getPriceCalcReqVO() {
        return priceCalcReqVO;
    }

    public void setPriceCalcReqVO(PriceCalcReqVO priceCalcReqVO) {
        this.priceCalcReqVO = priceCalcReqVO;
    }

    public List<PromotionPackVO> getPromotionPackList() {
        return promotionPackList;
    }

    public void setPromotionPackList(List<PromotionPackVO> promotionPackList) {
        this.promotionPackList = promotionPackList;
    }

    public BigDecimal getOriginalOrderPrice() {
        return originalOrderPrice;
    }

    public void setOriginalOrderPrice(BigDecimal originalOrderPrice) {
        this.originalOrderPrice = originalOrderPrice;
    }

    public BigDecimal getFinalOrderPrice() {
        return finalOrderPrice;
    }

    public void setFinalOrderPrice(BigDecimal finalOrderPrice) {
        this.finalOrderPrice = finalOrderPrice;
    }
}
