package com.future.bs.service;

import com.future.bs.bean.OrderContext;
import com.future.bs.bean.PriceCalcReqVO;
import com.future.bs.bean.ProductPackVO;
import com.future.bs.bean.PromotionInfoVO;
import com.future.bs.component.CalculateTotalOriginalPriceBusiness;
import com.future.bs.component.CheckItemCountBusiness;
import com.future.bs.component.CheckOrderChannelBusiness;
import com.future.bs.component.InitOrderContextBusiness;
import com.future.bs.component.MemberDiscountBusiness;
import com.future.bs.component.PreLockCouponsBusiness;
import com.future.bs.component.PreLockInventoryBusiness;
import com.future.bs.component.PromotionConvertBusiness;
import com.future.bs.component.report.Report;
import com.future.bs.component.stream.ConditionalStream;
import com.future.bs.component.stream.ParalleledStream;
import com.future.bs.component.stream.SequentialStream;
import com.future.bs.engine.DefaultBusinessEngine;
import com.future.bs.enums.CategoryEnum;
import com.future.bs.enums.OrderChannelEnum;
import com.future.bs.enums.PromotionTypeEnum;
import com.future.bs.enums.SkuSourceEnum;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>
 * Create Order Test
 * </p>
 *
 * @author Jingjie
 * @since 2020-09-26 16:26
 */
@Slf4j
public class CreateOrderTest {
    @Test
    public void createOrderSuccess() {
        OrderContext orderContext = new OrderContext();
        orderContext.setPriceCalcReqVO(mockReq());

        // 初始化阶段
        SequentialStream<OrderContext> initPhase = new SequentialStream<OrderContext>().new Builder()
                .name("init-phase")
                .process(new InitOrderContextBusiness())
                .build();

        // 校验阶段
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
        ParalleledStream<OrderContext> validatePhase = new ParalleledStream<OrderContext>().new Builder()
                .name("validate-phase")
                .process(Arrays.asList(new CheckOrderChannelBusiness(), new CheckItemCountBusiness()))
                .parallel(executorService)
                .build();


        // 执行阶段
        MemberDiscountBusiness memberDiscountBusiness = new MemberDiscountBusiness();
        ConditionalStream<OrderContext> conditionalStream = new ConditionalStream<OrderContext>()
                .new Builder()
                .name("conditional-stream")
                .process(p -> "M21152".equals(orderContext.getPriceCalcReqVO().getMemberCode()), memberDiscountBusiness)
                .build();
        SequentialStream<OrderContext> executePhase = new SequentialStream<OrderContext>().new Builder()
                .name("execute-phase")
                .process(new PreLockInventoryBusiness())
                .process(new PreLockCouponsBusiness())
                .process(new CalculateTotalOriginalPriceBusiness())
                .process(new PromotionConvertBusiness())
                .process(conditionalStream)
                .build();

        // 创单
        SequentialStream<OrderContext> createOrderStream = new SequentialStream<OrderContext>().new Builder()
                .name("create-order")
                .process(initPhase)
                .process(validatePhase)
                .process(executePhase)
                .build();

        Report<OrderContext> report = new DefaultBusinessEngine<OrderContext>().execute(createOrderStream, orderContext);
        log.info("最终价格：{}", report.getContext().getFinalOrderPrice());
        executorService.shutdown();
    }

    private PriceCalcReqVO mockReq() {
        PriceCalcReqVO req = new PriceCalcReqVO();
        req.setOrderNo("SO2020070611120001");
        req.setOversea(false);
        req.setMemberCode("M21152");
        req.setOrderChannel(OrderChannelEnum.APP);
        req.setCouponId(80081L);
        List<ProductPackVO> productPackList = new ArrayList<>();
        req.setProductPackList(productPackList);

        ProductPackVO productPack = new ProductPackVO();
        productPack.setProductId(5001L);
        productPack.setProductCode("PD5001XC");
        productPack.setSkuId(67001441L);
        productPack.setSkuCode("SKU5001XC001");
        productPack.setSkuName("夏季运动女式短裙M");
        productPack.setSkuSource(SkuSourceEnum.RAW);
        productPack.setCategory(CategoryEnum.CLOTHES);
        productPack.setSalePrice(new BigDecimal("139.00"));
        productPack.setCount(2);
        productPack.setPromotionList(new ArrayList<>(Arrays.asList(
                new PromotionInfoVO(1001L, "PM1001", "夏季满减活动", PromotionTypeEnum.FULL_CUT),
                new PromotionInfoVO(1002L, "PM1002", "夏季满折活动", PromotionTypeEnum.FULL_DISCOUNT))));
        productPackList.add(productPack);

        productPack = new ProductPackVO();
        productPack.setProductId(6001L);
        productPack.setProductCode("PD6001XC");
        productPack.setSkuId(67002334L);
        productPack.setSkuCode("SKU6001XC001");
        productPack.setSkuName("男士迷彩短袜均码");
        productPack.setSkuSource(SkuSourceEnum.RAW);
        productPack.setCategory(CategoryEnum.CLOTHES);
        productPack.setSalePrice(new BigDecimal("59.00"));
        productPack.setCount(3);
        productPack.setPromotionList(new ArrayList<>(Collections.singletonList(
                new PromotionInfoVO(1001L, "PM1001", "夏季满减活动", PromotionTypeEnum.FULL_CUT))));
        productPackList.add(productPack);

        productPack = new ProductPackVO();
        productPack.setProductId(8001L);
        productPack.setProductCode("PD8001XC");
        productPack.setSkuId(87002001L);
        productPack.setSkuCode("SKU8001XC001");
        productPack.setSkuName("纯棉毛巾");
        productPack.setSkuSource(SkuSourceEnum.RAW);
        productPack.setCategory(CategoryEnum.DAILY_USE);
        productPack.setSalePrice(new BigDecimal("28.00"));
        productPack.setCount(5);
        productPack.setPromotionList(new ArrayList<>(Collections.singletonList(
                new PromotionInfoVO(1002L, "PM1002", "夏季满折活动", PromotionTypeEnum.FULL_DISCOUNT))));
        productPackList.add(productPack);

        productPack = new ProductPackVO();
        productPack.setProductId(9001L);
        productPack.setProductCode("PD9001XC");
        productPack.setSkuId(97552001L);
        productPack.setSkuCode("SKU9001XC001");
        productPack.setSkuName("杀菌护手凝胶");
        productPack.setSkuSource(SkuSourceEnum.RAW);
        productPack.setCategory(CategoryEnum.DAILY_USE);
        productPack.setSalePrice(new BigDecimal("30"));
        productPack.setCount(2);
        productPack.setPromotionList(new ArrayList<>(Collections.singletonList(
                new PromotionInfoVO(1003L, "PM1003", "618抢购活动", PromotionTypeEnum.RUSH_BUY))));
        productPackList.add(productPack);

        return req;
    }
}
